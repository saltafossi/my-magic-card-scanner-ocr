"""This module provide HTTP endpoints"""
import os
from flask import Flask, request, abort, jsonify
from werkzeug.utils import secure_filename
import ocr_service
from config import Config

app = Flask(__name__)
app.config.from_object(Config)


@app.route("/scans", methods=["POST"])
def img_to_string():
    """This method updates files from external requests and
    checks the format before processing the image"""
    uploaded_file = request.files["file"]
    filename = secure_filename(uploaded_file.filename)
    if filename:
        file_ext = os.path.splitext(filename)[1]
        if file_ext not in app.config['UPLOAD_EXTENSIONS']:
            abort(404)
        uploaded_file.save(os.path.join(app.config['UPLOAD_PATH'], filename))
    return ocr_service.ocr_img(uploaded_file)


if __name__ == "__main__":
    app.run(host='0.0.0.0', port=8081)

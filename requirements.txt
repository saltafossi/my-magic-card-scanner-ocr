flask==2.2.2
pytesseract==0.3.10
Pillow==9.2.0
opencv-python==4.6.0.66
numpy==1.21.6
Werkzeug==2.2.2
pylint==2.15.4
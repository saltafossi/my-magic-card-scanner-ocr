FROM ubuntu:20.04

ARG DEBIAN_FRONTEND=noninteractive
ENV TZ=Europe/Paris
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

RUN apt-get update && \
	apt-get -y upgrade && \
    apt-get -y install python3.10 python3-pip && \
	apt-get clean && \
    apt-get autoremove

WORKDIR /app
COPY requirements.txt ./
COPY . .

RUN pip3 install -r requirements.txt

VOLUME ["/data"]
EXPOSE 8081 8081
CMD ["python3","app.py"]

"""This module provide image processing and cropping to scan card name"""
import re
import cv2
import numpy as np
import pytesseract
from PIL import Image
from pytesseract import Output
from card_scan_result import CardScanResult

# pytesseract.pytesseract.tesseract_cmd = 'C:\\Program Files\\Tesseract-OCR\\tesseract.exe'
TESSDATA_DIR_CONFIG = '--tessdata-dir ./tessdata'
WINDOW_NAME = 'image'
DEBUG_MODE = False


def test_contour_validity(contour, width, height):
    """Restricts contours to those that are possible"""
    area_valid = width * height * 0.1 <= cv2.contourArea(contour) <= width * height * 0.95

    rect = cv2.minAreaRect(contour)
    box_width = int(((rect[1][0] + rect[1][1]) / 2) * 0.95)
    box_height = int(((rect[1][0] + rect[1][1]) / 2) * 0.95)
    width_valid = width * 0.1 <= box_width <= width * 0.95
    height_valid = height * 0.1 <= box_height <= height * 0.95

    return area_valid and width_valid and height_valid


def find_square(img):
    """Find the largest square that is not the whole image"""
    img_height = np.size(img, 0)
    img_width = np.size(img, 1)
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    blur = cv2.GaussianBlur(gray, (1, 1), 1000)
    _, thresh = cv2.threshold(blur, 115, 255, cv2.THRESH_BINARY_INV)
    contours, _ = cv2.findContours(thresh, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
    debug = img.copy()
    max_contour = None
    for contour in contours:
        if test_contour_validity(contour, img_width, img_height):
            debug = cv2.drawContours(debug, [contour], -1, (0, 255, 0), 3)
            if max_contour is None or cv2.contourArea(max_contour) < cv2.contourArea(contour):
                max_contour = contour

    rect = cv2.minAreaRect(max_contour)
    box = cv2.boxPoints(rect)
    box = np.int0(box)
    return box


def dpot(point_1, point_2):
    """Some helper functions for distance calculations"""
    return (point_1 - point_2) ** 2


def adist(point1, point2):
    """Some helper functions for distance calculations"""
    return np.sqrt(dpot(point1[0], point2[0]) + dpot(point1[1], point2[1]))


def max_distance(point_a1, point_a2, point_b1, point_b2):
    """Some helper functions for distance calculations"""
    dist1 = adist(point_a1, point_a2)
    dist2 = adist(point_b1, point_b2)
    return int(dist1) if int(dist2) < int(dist1) else int(dist2)


def sort_points(points):
    """Sort points clockwise starting from top left"""
    sorted_points = np.zeros((4, 2), dtype="float32")
    sum_function = points.sum(axis=1)
    diff_function = np.diff(points, axis=1)
    sorted_points[0] = points[np.argmin(sum_function)]
    sorted_points[1] = points[np.argmin(diff_function)]
    sorted_points[2] = points[np.argmax(sum_function)]
    sorted_points[3] = points[np.argmax(diff_function)]
    return sorted_points


def fix_perspective(image, points):
    """Fix perspective"""
    (top_left, top_right, bottom_right, bottom_left) = sort_points(points)
    max_width = max_distance(bottom_right, bottom_left, top_right, top_left)
    max_height = max_distance(top_right, bottom_right, top_left, bottom_left)
    destination_points = np.array([[0, 0],
                                   [max_width - 1, 0],
                                   [max_width - 1, max_height - 1],
                                   [0, max_height - 1]],
                                   dtype="float32")
    transform = cv2.getPerspectiveTransform(np.array([top_left,
                                                      top_right,
                                                      bottom_right,
                                                      bottom_left],
                                                      dtype="float32"),
                                                      destination_points)
    fixed_image = cv2.warpPerspective(image, transform, (max_width, max_height))
    return fixed_image


def img_resize(img):
    """Resize the image to do a clean crop"""
    target_width = 672
    target_height = 936
    dim = (target_width, target_height)
    return cv2.resize(img, dim, interpolation=cv2.INTER_AREA)


def img_crop(img):
    """Crop the image to only get the set and collection number of the card"""
    y_start = int(img.shape[0] * 0.93)
    x_start = int(img.shape[1] * 0.05)
    crop_height = int(img.shape[0] * 0.23)
    crop_width = int(img.shape[1] * 0.39)
    return img[y_start:y_start + crop_height, x_start:x_start + crop_width]


def img_threshold(img):
    """Threshold the image, setting all foreground
    pixels to 255 (blanc) and all background pixels to 0 (noir)"""
    _, threshold = cv2.threshold(img, 0, 255, cv2.THRESH_BINARY | cv2.THRESH_OTSU)
    return threshold

def extract_collection_number(text):
    """Extracts the card's collection number (digits before the slash) from the text and removes trailing zeros"""
    collection_number = ""
    
    pattern = r"\b0*(\d{1})\d*/\d{3}\b"
    
    match = re.search(pattern, text)
    if match:
        collection_number = match.group(1)
    
    return collection_number



def extract_set(text):
    """Extracts the card's set from the text"""
    set = ""
    
    pattern = r"\d{3}/\d{3}\s*[A-Z]\s*([A-Z0-9]{3})"
    
    match = re.search(pattern, text)
    if match:
        set = match.group(1)
    
    return set.lower()


def ocr_img(file):
    """Function that processes the image using other functions"""
    img = np.array(Image.open(file))
    square = find_square(img)
    img = fix_perspective(img, square)
    resize = img_resize(img)
    grayscale = cv2.cvtColor(resize, cv2.COLOR_BGR2GRAY)
    crop = img_crop(grayscale)
    threshold = img_threshold(crop)
    img_txt = pytesseract.image_to_string(threshold,
                                          config="-c preserve_interword_spaces=1 --oem 3 --psm 6 -l eng+fra+mtg",
                                          lang="eng+fra+mtg",
                                          output_type=Output.STRING)
    img_txt_process = img_txt.replace("\n", "").replace("`\f", "")
    
    collection_number = extract_collection_number(img_txt_process)
    set = extract_set(img_txt_process)
    
    result = CardScanResult(collection_number, set)
    return result.to_dict()

class CardScanResult:
    def __init__(self, collection_number, set):
        self.collection_number = collection_number
        self.set = set
    
    def to_dict(self):
        return {
            'collection_number': self.collection_number,
            'set': self.set
        }